package epleSpillet;

public class V2D
{
	private double _x;
	private double _y;

	//Setter opp vektor
	void Init(double x, double y)
	{
		this._x = x;  
		this._y = y;
	}

	public V2D(V2D kopierFra)
	{
		Init(kopierFra._x, kopierFra._y);
	}
	//Lager 0 vektor
	public V2D()
	{
		Init(0, 0);
	}
	//lager vektor for x,y - koordinater
	public V2D(double x, double y)
	{
		Init(x, y);
	}
	//Ny vektor + annen vektor
	public V2D Pluss(V2D annen)
	{
		return new V2D(this._x + annen._x, this._y + annen._y);
	}

	public V2D Minus(V2D annen)
	{
		return new V2D(this._x - annen._x, this._y - annen._y);
	}

	public V2D Pluss(double annen)
	{
		return new V2D(this._x + annen, this._y + annen);
	}

	public V2D Minus(double annen)
	{
		return new V2D(this._x - annen, this._y - annen);
	}

	public V2D Ganger(double k)
	{
		return new V2D(k * this._x, k * this._y);
	}

	public V2D Deltpaa(double d)
	{
		return new V2D(this._x / d, this._y / d);
	}

	public double X()
	{
		return _x;
	}

	public double Y()
	{
		return _y;
	}

	public String toString()
	{
		return "(" + _x + ";" + _y + ")";
	}
}
