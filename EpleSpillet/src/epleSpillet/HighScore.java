package epleSpillet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class HighScore
{
	private static HighScore instance = null;
	private ArrayList<Score> highscore = new ArrayList<Score>();
	private String filnavn = "highscore.txt";

	public ArrayList<Score> getHighscore()
	{
		return highscore;
	}

	public void writeHighScore(String filename)		//skriver til fil
	{
		try {
			PrintWriter out = new PrintWriter(filename);
			for (Score s : highscore) {		//går gjennom og skriver til fil
				out.println(s.Navn() + ";" + s.Poeng());
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		//feilhåndtering
		}

	}
	//legger til score og skriver til fil
	public void addScore(Score s)
	{
		highscore.add(s);
		highscore.sort(null);
		writeHighScore(filnavn);
	}
	//leser highscore fra fil
	public void readHighScore(String filename)
	{
		BufferedReader br;

		try {
			highscore.clear();
			br = new BufferedReader(new FileReader(filename));
			String line;
						
			while ((line = br.readLine()) != null) {
				String[] deler = line.split(";");	//splitter linje i 2
				highscore.add(new Score(deler[0], Integer.parseInt(deler[1])));
			}
			br.close();
			highscore.sort(null);		//sorterer

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected HighScore()
	{
		try {
			File f = new File(filnavn);

			if (f.exists()) {
				readHighScore(filnavn);
			} else {
				highscore.add(new Score("Ingrid", 2000));
				highscore.add(new Score("Dag", 1800));
				highscore.add(new Score("Kim", 1600));
				highscore.add(new Score("Ivar", 600));
				highscore.add(new Score("Kjell", 1500));
				highscore.sort(null);
				writeHighScore(filnavn);
			}
		} catch (Exception ex) {

		}

	}
	//får tak i singleton
	public static HighScore getInstance()
	{
		if (instance == null) {
			instance = new HighScore();
		}
		return instance;
	}

}
