package epleSpillet;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Start extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 4841961918895878859L;
	JButton nyttSpill = new JButton("Nytt Spill");
	JButton highScore = new JButton("Se Highscore");
	JButton lukk = new JButton("Lukk Spillet");

	public Start()
	{
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		this.add(nyttSpill);
		nyttSpill.addActionListener(this);
		this.add(highScore);
		highScore.addActionListener(this);
		this.setBounds(600, 200, 50, 1000);
		this.add(lukk);
		lukk.addActionListener(this);
		this.setSize(130, 160);
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		if (e.getSource().equals(nyttSpill)) {
			String navn = JOptionPane.showInputDialog(this, "Hva er navnet ditt?");
			SpillVindu t = new SpillVindu(navn);
			JFrame jf = new JFrame();
			jf.setTitle("Eplespillet");
			jf.setSize(800, 600);
			jf.setVisible(true);
			jf.add(t);
		}
		if (e.getSource().equals(highScore)) {
			int i = 1;
			String tekst = "High Score: \n\n";
			for(Score s : HighScore.getInstance().getHighscore()) {
				tekst+=i+". "+ s+"\n";
				i++;
			}
			JOptionPane.showMessageDialog(this, tekst);
		}
		if (e.getSource().equals(lukk)) {
			setVisible(false);
			dispose();
		}
	}

}
