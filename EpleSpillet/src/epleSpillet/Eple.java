package epleSpillet;

public class Eple
{
	public enum EpleTilstander {
		INAKTIVT, VENTENDE, VOKSENDE, FALLENDE
	}

	private V2D _pos;
	private V2D _fart;
	private V2D _aks;
	private double _timer;
	private EpleTilstander _tilstand;
	private double _storrelse;
	private boolean _daarlig;
	private boolean _synlig;

	private void Init(V2D pos, double timer, boolean daarlig)
	{
		_pos = pos;
		_fart = new V2D(); // lager nullvektor
		_aks = new V2D(0, 100); // 100 piksler pr sek^2
		_tilstand = EpleTilstander.VENTENDE; // forskjellige tilstander
							// på eplene
		_timer = timer;
		_storrelse = 20; // setter størrelse 20
		_daarlig = daarlig;
		_synlig = false; // synlighet på eple
	}

	public Eple()
	{
		Init(new V2D(), 0, false); // lager eple i pos 0 // konstruktør
						// uten arg.
	}

	public Eple(V2D pos, double timer) // godt eple med posisjon og timer
						// angitt
	{
		Init(pos, timer, false);
	}

	// angitt posisjon, timer og om det er dårlig
	public Eple(V2D pos, double timer, boolean daarlig)
	{
		Init(pos, timer, daarlig);
	}

	// utfører 1 iterasjon av fysikksimulering
	public void Iterer(double dt) // dt = antall sek. passert siden sist
					// kall til funks.
	{
		switch (_tilstand) {
		case VENTENDE:
			_timer -= dt;
			if (_timer <= 0) {
				_storrelse = 0;
				_timer = 5;
				_tilstand = EpleTilstander.VOKSENDE; // etter
									// timer
									// ferdig
									// går
									// til
				// neste tilstand..Synlig
				_synlig = true;
			}
			break;
		case VOKSENDE:
			_timer -= dt;
			if (_timer <= 0) {
				_tilstand = EpleTilstander.FALLENDE;
			} else {
				_storrelse += 4 * dt;
			}
			break;
		case FALLENDE: // fart og posisjon blir beregnet
			_fart = _fart.Pluss(_aks.Ganger(dt));
			_pos = _pos.Pluss(_fart.Ganger(dt));
			break;
		case INAKTIVT: // skjer ingenting
			break;
		}
	}

	public int X()
	{
		return (int) _pos.X();
	}

	public int Y()
	{
		return (int) _pos.Y();
	}

	public boolean Daarlig()
	{
		return _daarlig;
	}

	public int Storrelse()
	{
		return (int) _storrelse;
	}

	public boolean Synlig()
	{
		return _synlig;
	}

	public String toString()
	{
		return _pos.toString();
	}

	public EpleTilstander Tilstand()
	{
		return _tilstand;
	}

	// For å deaktivere eple
	public void Ferdig()
	{
		_pos = new V2D();
		_synlig = false;
		_tilstand = EpleTilstander.INAKTIVT;
	}
}
