package epleSpillet;

public class Score implements Comparable<Score>
{
	private String _navn;
	private int _poeng;

	private void Init(String navn, int poeng)
	{
		_navn = navn;
		_poeng = poeng;
	}

	public Score()
	{
		Init("", 0);
	}

	public Score(String navn, int poeng)
	{
		Init(navn, poeng);
	}

	public String toString()
	{
		return _navn + "   " + _poeng;
	}

	public String Navn() {return _navn;}
	public int Poeng() {return _poeng;}
	
	public int compareTo(Score s)
	{
		return s._poeng - _poeng;
	}

}
