package epleSpillet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import epleSpillet.Eple.EpleTilstander;

public class SpillVindu extends JPanel implements ActionListener, MouseMotionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8390913912629923798L;
	Random _rng;		//tilfeldige tall
	Timer _timer;
	long _millisLast;	//antall millisek. siden sist kall
	ArrayList<Eple> _epler = new ArrayList<Eple>();
	V2D _kurv;		//lager posisjon til kurv
	String _spillernavn;	//lager spiller navn
	int _poeng;		
	int _eplerigjen;
	boolean _gameover;	//tilstand til om spill er over
	double _fps = 0;
	/**
	 * @param args
	 */
	//Setter opp nytt spill
	//stiller variabler til fornuftige verdier
	public SpillVindu(String spillerNavn)
	{
		_rng = new Random();
		_millisLast = System.currentTimeMillis();
		_spillernavn = spillerNavn;
		_poeng = 0;
		_timer = new Timer(5, this);
		_timer.start();
		_gameover = false;
		setSize(800, 600);
		//lager epler
		for(int i = 0; i < 100; i++) {
			_epler.add(new Eple(new V2D(_rng.nextInt(this.getWidth()-10), _rng.nextInt(this.getHeight()/2)), Math.sqrt(i)*10.0, _rng.nextInt(10)==0));
		}
		
		/*for(int i = 0; i < 1000; i++) {
			_epler.add(new Eple(new V2D(this.getWidth()/2, this.getHeight()/2), 10.0, false));
		}*/
		//teller antall epler som er blitt lagt
		_eplerigjen = _epler.size();
		
		//Kurven blir satt på midten
		_kurv = new V2D(this.getWidth() / 2, this.getHeight() - 1);
		this.addMouseMotionListener(this);
	}
	//Tegning av all grafikk
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		
		//går gjennom alle epler og tegner de synlige
		for (Eple e : _epler) {
			if (e.Synlig()) {
				if(e.Tilstand() == EpleTilstander.VOKSENDE) {
					g.setColor(Color.GREEN);
				} else  if (e.Daarlig()) {
					g.setColor(new Color(128, 64, 0));
				} else {
					g.setColor(Color.RED);
				}
				
				g.fillOval(e.X() - e.Storrelse() / 2, e.Y(), e.Storrelse(), e.Storrelse());
			}
		}
		g.setColor(Color.YELLOW);
		g.fillRect((int) (_kurv.X() - 30), (int) (_kurv.Y() - 30), 60, 30); //sentrerer rundt musepeker
		g.setColor(Color.BLACK);
		//Skriver ut tekst
		g.drawString("Navn: " + _spillernavn, 1, 10);
		g.drawString("Poeng: " + _poeng, 1, 25);
		g.drawString("Epler igjen: " + _eplerigjen, 1, 40);
		g.drawString("FPS: " + (int)_fps, 1, 55);
	}

	public void actionPerformed(ActionEvent e)
	{
		double dt;
		long now = System.currentTimeMillis();
		dt = (now - _millisLast) / 1000.0;
		_fps = 1.0/dt;
		_millisLast = now;

		//går gjennom for å behandle fysikken for hvert eple
		for (Eple ep : _epler) {
			ep.Iterer(dt);
			if (ep.Y() > this.getWidth()){
				ep.Ferdig();
				_eplerigjen--;
			}
			if (ep.Y() > _kurv.Y() - 30) {
				if ((ep.X() > _kurv.X() - 30) && (ep.X() < _kurv.X() + 30)) {
					if (ep.Daarlig()) {
						_poeng -= 1000;
					} else {
						_poeng += 100;
					}
					_eplerigjen--;
					ep.Ferdig();
					
				}
			}
		}
		if(_eplerigjen==0 && _gameover == false){
			_gameover = true;
			JOptionPane.showMessageDialog(this, "Spillet er over");
			HighScore.getInstance().addScore(new Score(_spillernavn, _poeng));
		}
		repaint();
	}

	public void mouseMoved(MouseEvent e)
	{
		_kurv = new V2D(e.getX(), this.getHeight());
	}

	public void mouseDragged(MouseEvent e)
	{
	}
}
